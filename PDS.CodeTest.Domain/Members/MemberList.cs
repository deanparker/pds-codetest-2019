﻿using System.Xml.Serialization;

namespace PDS.CodeTest.Domain.Members
{
    [XmlRoot(ElementName = "Members")]
    public class MemberList
    {
        public Member Member { get; set; }
    }
}