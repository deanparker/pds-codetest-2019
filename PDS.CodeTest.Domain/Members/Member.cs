﻿using System.Xml.Serialization;

namespace PDS.CodeTest.Domain.Members
{
    public class Member
    {
        [XmlAttribute(AttributeName = "Member_Id")]
        public int ID { get; set; }
        public string FullTitle { get; set; }
        public string Party { get; set; }
        public string MemberFrom { get; set; }
    }
}