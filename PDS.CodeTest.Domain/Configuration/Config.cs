﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDS.CodeTest.Domain.Configuration
{
    public class Config
    {
        public string CalendarUrl { get; set; }
        public string MembersNamesUrl { get; set; }
        public string Type { get; set; }
    }
}
