﻿namespace PDS.CodeTest.Domain.Events
{
    public class EventMember
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortOrder { get; set; }
    }
}