"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
var _ = require("lodash");
var EventService_1 = require("./EventService");
var EventBuilder_1 = require("./EventBuilder");
var ModalService_1 = require("./ModalService");
var TableBuilder_1 = require("./TableBuilder");
var Main = /** @class */ (function () {
    function Main(eventBuilder, modalService) {
        this.eventBuilder = eventBuilder;
        this.modalService = modalService;
    }
    Main.prototype.init = function () {
        var _this = this;
        var weekBeginning = document.querySelector("#WeekBeginning");
        weekBeginning.addEventListener("keyup", _.debounce(function (event) { return _this.updateTable(weekBeginning.value); }, 300));
        document.addEventListener("click", function (event) {
            var element = event.target;
            if (element.matches("td")) {
                var row = element.parentElement;
                _this.showModal(row);
            }
            ;
        }, false);
        this.updateTable(weekBeginning.value);
    };
    Main.prototype.updateTable = function (weekBeginning) {
        var date = moment(weekBeginning, "DD/MM/YYYY");
        if (date.isValid()) {
            this.eventBuilder.buildEventTable(weekBeginning);
        }
    };
    Main.prototype.showModal = function (row) {
        var description = row.dataset.description;
        var category = row.dataset.category;
        var members = JSON.parse(row.dataset.members);
        this.modalService.showModal(description, category, members);
    };
    return Main;
}());
exports.Main = Main;
(function () {
    var eventService = new EventService_1.EventService();
    var tableBuilder = new TableBuilder_1.TableBuilder();
    var eventBuilder = new EventBuilder_1.EventBuilder(eventService, tableBuilder);
    var modalService = new ModalService_1.ModalService(tableBuilder);
    var main = new Main(eventBuilder, modalService);
    main.init();
})();
//# sourceMappingURL=main.js.map