"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ModalService = /** @class */ (function () {
    function ModalService(tableBuilder) {
        this.tableBuilder = tableBuilder;
    }
    ModalService.prototype.showModal = function (description, category, members) {
        var categoryElement = document.querySelector("#category"), descriptionElement = document.querySelector("#description");
        descriptionElement.textContent = description;
        categoryElement.textContent = category;
        this.buildTable(members);
    };
    ModalService.prototype.buildTable = function (members) {
        var _this = this;
        var tbody = document.querySelector("#members > tbody");
        this.tableBuilder.clear(tbody);
        if (members.length) {
            members.map(function (member) { return _this.buildRow(member); })
                .forEach(function (row) { return tbody.append(row); });
        }
        else {
            tbody.append(this.buildEmptyRow());
        }
    };
    ModalService.prototype.buildRow = function (member) {
        var row = document.createElement("tr");
        var fullTitle = this.tableBuilder.createCell(member.fullTitle);
        var constituency = this.tableBuilder.createCell(member.constituency);
        var party = this.tableBuilder.createCell(member.party);
        row.append(fullTitle);
        row.append(constituency);
        row.append(party);
        return row;
    };
    ModalService.prototype.buildEmptyRow = function () {
        var row = document.createElement("tr");
        var none = this.tableBuilder.createCell("No members to display");
        none.colSpan = 3;
        row.append(none);
        return row;
    };
    return ModalService;
}());
exports.ModalService = ModalService;
//# sourceMappingURL=ModalService.js.map