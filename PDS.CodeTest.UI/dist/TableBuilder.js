"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TableBuilder = /** @class */ (function () {
    function TableBuilder() {
    }
    TableBuilder.prototype.clear = function (table) {
        $(table).empty();
    };
    TableBuilder.prototype.createCell = function (text) {
        var cell = document.createElement("td");
        cell.innerText = text;
        return cell;
    };
    return TableBuilder;
}());
exports.TableBuilder = TableBuilder;
//# sourceMappingURL=TableBuilder.js.map