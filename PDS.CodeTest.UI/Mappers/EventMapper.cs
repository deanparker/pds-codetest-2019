﻿using PDS.CodeTest.Domain.Events;
using PDS.CodeTest.Domain.Members;
using PDS.CodeTest.Services.Members;
using PDS.CodeTest.UI.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PDS.CodeTest.UI.Mappers
{
    public class EventMapper : IEventMapper
    {
        private readonly IMemberService _memberService;

        public EventMapper(IMemberService memberService)
        {
            _memberService = memberService;
        }

        public async Task<EventViewModel> MapToEventViewModelAsync(Event @event)
        {
            var members = await Task.WhenAll(@event.Members.Select(m => _memberService.GetByIdAsync(m.Id)));

            var model = new EventViewModel()
            {
                Description = @event.Description,
                Category = @event.Category,

                StartDate = @event.StartDate,
                StartTime = @event.StartTime,
                EndDate = @event.EndDate,
                EndTime = @event.EndTime,

                Members = members.Select(MapToMemberViewModel)
            };

            return model;
        }

        private MemberViewModel MapToMemberViewModel(Member member)
        {
            return new MemberViewModel()
            {
                FullTitle = member.FullTitle,
                Party = member.Party,
                Constituency = member.MemberFrom
            };
        }
    }
}
