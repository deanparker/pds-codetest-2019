﻿using PDS.CodeTest.Domain.Events;
using PDS.CodeTest.UI.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PDS.CodeTest.UI.Mappers
{
    public interface IEventMapper
    {
        Task<EventViewModel> MapToEventViewModelAsync(Event @event);
    }
}
