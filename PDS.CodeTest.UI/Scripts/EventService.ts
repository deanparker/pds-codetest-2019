﻿import * as moment from 'moment';
import { Event } from './Models/Event';

export class EventService {
    public async getEvents(weekBeginning: string | Date): Promise<Array<Event>> {
        let date = moment(weekBeginning, "DD/MM/YYYY").format("YYYY-MM-DD");

        return await $.getJSON(`/api/events/get?weekBeginning=${date}`);
    }
}