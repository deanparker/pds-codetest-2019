﻿import { TableBuilder } from "./TableBuilder";
import { Member } from "./Models/Member";

export class ModalService {
    constructor(private tableBuilder: TableBuilder) { }

    public showModal(description: string, category: string, members: Member[]) {
        let categoryElement = document.querySelector("#category"), descriptionElement = document.querySelector("#description");

        descriptionElement.textContent = description;
        categoryElement.textContent = category;

        this.buildTable(members);
    }

    private buildTable(members: Member[]) {
        let tbody = document.querySelector("#members > tbody");

        this.tableBuilder.clear(tbody);

        if (members.length) {

            members.map(member => this.buildRow(member))
                .forEach(row => tbody.append(row));
        } else {
            tbody.append(this.buildEmptyRow());
        }
    }

    private buildRow(member: Member) : Element {
        let row = document.createElement("tr");

        let fullTitle = this.tableBuilder.createCell(member.fullTitle);
        let constituency = this.tableBuilder.createCell(member.constituency);
        let party = this.tableBuilder.createCell(member.party);

        row.append(fullTitle);
        row.append(constituency);
        row.append(party);

        return row
    }

    private buildEmptyRow(): Element {
        let row = document.createElement("tr");

        let none = this.tableBuilder.createCell("No members to display");

        none.colSpan = 3;
        row.append(none);

        return row
    }
}