﻿import { EventService } from "./EventService";
import { TableBuilder } from "./TableBuilder";
import { Event } from "./Models/Event";

export class EventBuilder {
    constructor(private eventService: EventService, private tableBuilder : TableBuilder) {
    }

    public async buildEventTable(weekBeginning: string | Date) {
        var events = await this.eventService.getEvents(weekBeginning);

        var evts = document.querySelector("#events > tbody");

        this.tableBuilder.clear(evts);

        if (events.length) {
            events.map(event => this.buildRow(event)).forEach(row => evts.appendChild(row));
        } else {
            evts.append(this.buildEmptyRow());
        }
    }

    private buildRow(event: Event) {
        let row = this.createRow();

        let description = this.tableBuilder.createCell(event.description);
        let start = this.tableBuilder.createCell(event.start);
        let end = this.tableBuilder.createCell(event.end);

        row.append(description);
        row.append(start);
        row.append(end);

        row.dataset.description = event.description;
        row.dataset.category = event.category;
        row.dataset.members = JSON.stringify(event.members);

        return row;
    }

    private createRow(): HTMLTableRowElement {
        let row = document.createElement("tr");

        row.dataset.toggle = "modal";
        row.dataset.target = "#details";

        return row;
    }

    private buildEmptyRow(): HTMLTableRowElement {
        let row = document.createElement("tr");

        let none = this.tableBuilder.createCell("No events to display");

        none.colSpan = 3;
        row.append(none);

        return row
    }
}