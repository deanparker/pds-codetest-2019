﻿import * as moment from 'moment';
import * as _ from 'lodash';
import { EventService } from './EventService';
import { EventBuilder } from './EventBuilder';
import { ModalService } from './ModalService';
import { TableBuilder } from './TableBuilder';
import { Member } from './Models/Member';

export class Main {
    constructor(private eventBuilder: EventBuilder, private modalService: ModalService) { }

    public init() {
        let weekBeginning: HTMLInputElement = document.querySelector("#WeekBeginning");

        weekBeginning.addEventListener("keyup", _.debounce(event => this.updateTable(weekBeginning.value), 300));
        document.addEventListener("click", event => {
            let element = (event.target as Element);

            if (element.matches("td")) {
                let row = element.parentElement as HTMLTableRowElement;

                this.showModal(row);
            };
        }, false);

        this.updateTable(weekBeginning.value);
    }

    private updateTable(weekBeginning) {
        let date = moment(weekBeginning, "DD/MM/YYYY");


        if (date.isValid()) {
            this.eventBuilder.buildEventTable(weekBeginning);
        }
    }

    private showModal(row: HTMLTableRowElement) {
        let description = row.dataset.description;
        let category = row.dataset.category;
        let members: Member[] = JSON.parse(row.dataset.members);

        this.modalService.showModal(description, category, members);
    }
}


(function () {
    let eventService = new EventService();
    let tableBuilder = new TableBuilder();

    let eventBuilder = new EventBuilder(eventService, tableBuilder);
    let modalService = new ModalService(tableBuilder);

    let main = new Main(eventBuilder, modalService);

    main.init();
})();
