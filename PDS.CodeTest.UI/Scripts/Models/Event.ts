﻿import { Member } from "./Member";

export class Event {
    public category: string;
    public description: string;
    public members: Member[];
    public start: string;
    public end: string;
}