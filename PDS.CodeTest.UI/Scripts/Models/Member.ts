﻿export class Member {
    public fullTitle: string;
    public constituency: string;
    public party: string;
}