﻿export class TableBuilder {

    public clear(table: Element) {
        $(table).empty();
    }

    public createCell(text: string): HTMLTableDataCellElement {
        let cell = document.createElement("td");

        cell.innerText = text;

        return cell;
    }
}