﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Collections.Generic;
using PDS.CodeTest.Services.Calendar;
using PDS.CodeTest.UI.Models;
using PDS.CodeTest.UI.Mappers;

namespace PDS.CodeTest.UI.Controllers
{
    [Route("api/[controller]"), ApiController]
    public class EventsController : ControllerBase
    {
        private readonly ICalendarService _calendarService;
        private readonly IEventMapper _eventMapper;

        public EventsController(ICalendarService calendarService, IEventMapper eventMapper)
        {
            _calendarService = calendarService;
            _eventMapper = eventMapper;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<EventViewModel>> Get(DateTime weekBeginning)
        {
            var events = await _calendarService.GetEvents(weekBeginning, weekBeginning.AddDays(5));

            var models = await Task.WhenAll(events.Select(_eventMapper.MapToEventViewModelAsync));

            return models.OrderBy(ord => ord.StartDate).ThenBy(ord => ord.StartTime);
        }
    }
}