﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using PDS.CodeTest.Models;
using PDS.CodeTest.UI.Models;

namespace PDS.CodeTest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var date = DateTime.Now.Date;

            while(date.DayOfWeek != DayOfWeek.Monday)
            {
                date = date.AddDays(-1);
            }

            var model = new HomeViewModel()
            {
                WeekBeginning = date
            };

            return View(model);
        }
 
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
