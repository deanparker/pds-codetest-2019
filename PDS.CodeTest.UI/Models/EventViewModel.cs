﻿using System;
using System.Collections.Generic;

namespace PDS.CodeTest.UI.Models
{
    public class EventViewModel
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }

        public IEnumerable<MemberViewModel> Members { get; set; }

        public string Start => $"{StartDate:d} {StartTime}";
        public string End => $"{EndDate:d} {EndTime}";
    }
}
