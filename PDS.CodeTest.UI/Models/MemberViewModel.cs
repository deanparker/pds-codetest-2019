﻿namespace PDS.CodeTest.UI.Models
{
    public class MemberViewModel
    {
        public string FullTitle { get; set; }
        public string Party { get; set; }
        public string Constituency { get; set; }
    }
}
