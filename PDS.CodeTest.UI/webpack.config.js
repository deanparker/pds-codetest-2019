﻿const path = require('path');

module.exports = {
    mode: 'development',
    entry: './Scripts/main.ts',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js"]
    },
    output: {
        path: path.resolve(__dirname, 'wwwroot/js'),
        filename: '[name].js'
    }
};