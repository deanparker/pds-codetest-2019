﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace PDS.CodeTest.Services
{
    public class Content
    {
        private static async Task<WebResponse> GetAsync(string url)
        {
            var request = WebRequest.Create(url);

            return await request.GetResponseAsync();
        }

        public static async Task<T> GetJsonAsync<T>(string url)
        {
            var response = await GetAsync(url);

            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                var responseString = await reader.ReadToEndAsync();

                return JsonConvert.DeserializeObject<T>(responseString);
            }
        }

        public static async Task<T> GetXmlAsync<T>(string url)
        {
            var response = await GetAsync(url);

            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                var serialiser = new XmlSerializer(typeof(T));
                return (T)serialiser.Deserialize(reader);
            }
        }
    }
}
