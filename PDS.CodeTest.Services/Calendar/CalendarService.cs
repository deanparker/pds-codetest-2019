using System;
using System.Collections.Generic;
using PDS.CodeTest.Domain.Events;
using System.Threading.Tasks;
using System.Linq;
using PDS.CodeTest.Domain.Configuration;
using Microsoft.Extensions.Options;

namespace PDS.CodeTest.Services.Calendar
{
    public class CalendarService : ICalendarService
    {
        private readonly string _type, _url;

        public CalendarService(IOptions<Config> config)
        {
            _type = config.Value.Type;
            _url = config.Value.CalendarUrl;
        }

        public async Task<IEnumerable<Event>> GetEvents(DateTime startDate, DateTime endDate)
        {
            var url = $"{_url}/list.json?house=Commons&startdate={startDate:yyyy-MM-dd}&enddate={endDate:yyyy-MM-dd}";

            var events = await Content.GetJsonAsync<IEnumerable<Event>>(url);

            return events.Where(e => e.Type == _type);
        }
    }
}