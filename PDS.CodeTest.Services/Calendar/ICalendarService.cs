using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PDS.CodeTest.Domain.Events;

namespace PDS.CodeTest.Services.Calendar
{
    public interface ICalendarService
    {
        Task<IEnumerable<Event>> GetEvents(DateTime dateStart, DateTime endDate);
    }
}