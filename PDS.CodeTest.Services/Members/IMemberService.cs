using PDS.CodeTest.Domain.Members;
using System.Threading.Tasks;

namespace PDS.CodeTest.Services.Members
{
    public interface IMemberService
    {
        Task<Member> GetByIdAsync(int id);        
    }
}