using Microsoft.Extensions.Options;
using PDS.CodeTest.Domain.Configuration;
using PDS.CodeTest.Domain.Members;
using System.Threading.Tasks;

namespace PDS.CodeTest.Services.Members
{
    public class MemberService : IMemberService
    {
        private readonly string _url = "http://data.parliament.uk/membersdataplatform/services/mnis/members/query";

        public MemberService(IOptions<Config> configOptions)
        {
            _url = configOptions.Value.MembersNamesUrl;
        }

        public async Task<Member> GetByIdAsync(int id)
        {
            var url = $"{_url}/id={id}/";

            var members = await Content.GetXmlAsync<MemberList>(url);

            return members.Member;
        }
    }
}
